# Greasemonkey Scripts

My Greasemonkey scripts for Firefox.

### Better Google Search Tools

Bring language option back

v.1.0.4

- require Google Search Tools Back, https://userscripts.org/scripts/show/152796

v.1.0.3

- Bring Google search tools back to left sidebar
- Add most used language options as well
- Works fine with Auto Pager





![Search](https://dl.dropbox.com/u/171095/Screenshots/bgst-sample-search.png)
![Images](https://dl.dropbox.com/u/171095/Screenshots/bgst-sample-image.png)
