// ==UserScript==
// @name        Better Goole Search Tools
// @namespace   ETHAN/BGST
// @description Bring Google search tools and language optios back.
// @include     http://*.google.*
// @include     https://*.google.*
// @version     2.0
// @require     http://code.jquery.com/jquery-1.11.1.min.js
// @run-at      document-end
// @grant       none
// ==/UserScript==


this.$ = this.jQuery = jQuery.noConflict(true);

var lang = {
    "lang_en": "英",
    "lang_zh-TW": "繁",
    "lang_zh-CN": "简",
    "lang_ja": "日"
};

var url = window.location.protocol + '//' + window.location.hostname + '/search?hl=en&newwindow=1';
var search = window.location.search.substring(1).split('&');
var current_lr = '';
var search_target = '';

// get search string
for (var i=0; i < search.length; i++) {
    var param = search[i].split('=');
    if (param[0] === 'lr') {
        current_lr = param[1];
    }
    if (param[0] === 'q') {
        url += '&q=' + param[1];
    }
    if (param[0] === 'tbm' && param[1] === 'isch') {
        search_target = 'image';
    }
};

// add language option
for (var lr in lang){
    var link = url + '&lr=' + lr;

    var item = '<div class="hdtb-mitem hdtb-imb"><a class="q qs" href="' + link + '">' + lang[lr] + '</a></div>';
    jQuery('#hdtb-msb').append(item);

};

